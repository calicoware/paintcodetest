//
//  DrawClass.m
//  PaintCodeAppTest
//
//  Created by Chris Barker on 14/09/2014.
//  Copyright (c) 2014 CalicoWare. All rights reserved.
//

#import "DrawClass.h"
#import "StyleKitShapes.h"

@implementation DrawClass


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    [StyleKitShapes drawStarGreen];
    
}


@end
