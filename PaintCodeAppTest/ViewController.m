//
//  ViewController.m
//  PaintCodeAppTest
//
//  Created by Chris Barker on 14/09/2014.
//  Copyright (c) 2014 CalicoWare. All rights reserved.
//

#import "ViewController.h"
#import "StyleKitShapes.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.    
    
    self.label.textColor = [StyleKitShapes color];
    self.image.image = [StyleKitShapes imageOfStarGreen];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
